RGA_Min - Final Version 1.0.1 (final_1.0.1)

INSTALLATION:
For installation on the system use setup.py:
	-python setup.py install --user (for installation only for this user)

USAGE:
This package is used to read in the raw peak and analog files of mass spectrometers on the AFS system of ASDEX Upgrade and give back easy accessible data. 

HOW TO USE IT:
Use function Peak_Jump_Data as following:
	from RGA_Min.py import Peak_Jump_Data

	timetrace_peak, first_spectrum, timetrace_analog, error_code = Peak_Jump_Data(device, date, time, shotnumber)

where you have to specify a specific device as a string (like 'HPQI', 'HPQO', 'HPQO2', 'MV', 'HD', 'HIDEN', 'HIDEN_2') and date and time of the shot OR the shotnumber (as string). For example:

	timetrace_peak, first_spectrum, timetrace_analog, error_code = Peak_Jump_Data('HPQI', shotnumber = '34800')
or
	timetrace_peak, first_spectrum, timetrace_analog, error_code = Peak_Jump_Data('HPQI', date = '12.10.2017', time = '13:50:00').

this will give you back the timetraces of the analog and peak data, the first measured spectrum of the analog data and error codes, which can be seen in the Error.txt file.

If errors occur or any questions arise write a mail at:
thomas.reichbauer@ipp.mpg.de
