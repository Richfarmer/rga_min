import time as t
import datetime as dt
import numpy as np

from rga_import.SETTINGS import *
from rga_import.Get_Date_from_Shotnumber import get_date_time
from rga_import.Search_Raw_Data_min import search_files_for_one_device
from rga_import.check_input import right_input
import rga_import.Corrections as c

version = 'final_1.0.1'      

    
def Peak_Jump_Data(device, date = None, time = None, shotnumber = None, max_timeoffset_shottopeak = 480, max_timeoffset_peaktoanalog = 200):
    error = []
    error.extend(right_input(device, date, time, shotnumber))
    '''If any error in input: crucial error (all 35x errors)'''
    if error != []:
        return {}, [], {}, error

    if shotnumber != None and date == None and time == None:
        date, time, err = get_date_time(int(shotnumber))
        '''If curcial error occured while getting a date/time: stop and give back error'''
        error.extend(err)
        if any(x in error for x in [151, 152]):
            return {}, [], {}, error

    peak, analog, err = search_files_for_one_device(device, date, time, max_timeoffset_shottopeak = max_timeoffset_shottopeak, max_timeoffset_peaktoanalog = max_timeoffset_peaktoanalog)
    error.extend(err)

    '''If no data found give back error'''
    if 150 in error:
        return {}, [], {}, error
    '''Get data of Peak'''
    s, t, h, err = functions[device][0](peak[0])

    error.extend(err)
    tt_peak = t

    '''Get data of Analog'''
    if 250 not in error:
        s_all = []
        t_all = []
        h_all = []
        for i in analog:
            for k in xrange(len(functions[device][1])):
                try:
                    s, t, h, err = functions[device][1][k](i)
                except:
                    pass
            s_all.append(s)
            t_all.append(t)
            h_all.append(h)
            error.extend(err)

        t_end = {}
        s_end = []
        h_end = {}
        '''If 2 analog views: use time/date from first one'''
        if len(s_all) > 1: 
            if len(t_all[0]) < len(t_all[1]): # 0 is then 1-6
                if len(t_all[0]['time']) != len(t_all[1]['time']):
                    for k, v in t_all[0].iteritems():
                        if type(k) == int:
                            t_all[0][k] = t_all[0][k][:-1]
                t_end = t_all[0]
                for k, v in t_all[1].iteritems():
                    if type(k) == int:
                        t_end[k] = t_all[1][k]
                        
                s_end = s_all[0][0]
                s_end[-2] = np.hstack((s_end[-2][:-1], s_all[1][0][-2]))
                s_end[-1] = np.hstack((s_end[-1][:-1], s_all[1][0][-1]))
                        
            else: # Then 1 is 1-6
                if len(t_all[1]['time']) != len(t_all[0]['time']):
                    for k, v in t_all[1].iteritems():
                        if type(k) == int:
                            t_all[1][k] = t_all[1][k][:-1]
                t_end = t_all[1]
                for k, v in t_all[0].iteritems():
                    if type(k) == int:
                        t_end[k] = t_all[0][k]
                        
                s_end = s_all[1][0]
                s_end[-2] = np.hstack((s_end[-2][:-1], s_all[0][0][-2]))
                s_end[-1] = np.hstack((s_end[-1][:-1], s_all[0][0][-1]))
                
            spec = s_end
                
        else:
            t_end = t_all[0]
            s_end = s_all[0]
            h_end = h_all[0]
            try:
                spec = s_end[0]
            except:
                spec = s_end

        tt_analog = t_end
        
        '''Corrections of Analog file: Set smallest negative value to 0'''
        tt_analog = c.correct_TT(tt_analog)
        spec = c.correct_S(spec)
    else:
        tt_analog = {}
        spec = []

    return tt_peak, spec, tt_analog, error


#print Peak_Jump_Data('HIDEN',date = '01.04.2016', time = '08:40:00')
#t1,s,t2,e = Peak_Jump_Data('HD', shotnumber = '34764')
#print t2['hours_raw']
#print Peak_Jump_Data('HIDEN', shotnumber = '33025')
