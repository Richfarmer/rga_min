# -*- coding: utf-8 -*-
def correct_TT(tt):
    try:
        for key, value in tt.iteritems():
            if type(key) == int:
                smallest = min(tt[key])
                if smallest < 0:
                    tt[key] = map(lambda x: x- smallest, tt[key])
        return tt
    except:
        return tt


def correct_S(s):
    try:
        smallest = min(s[7])
        if smallest < 0:
            s[7] = map(lambda x: x- smallest, s[7])
        return s
    except:
        return s
    
    
    

