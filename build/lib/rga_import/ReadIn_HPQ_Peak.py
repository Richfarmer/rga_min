# -*- coding: utf-8 -*-
"""
"""
import numpy as np


def read_HPQ_P(file_path):
    """
    INPUT: File path 
    OUTPUT: 1. List of spectra: All spectra contain:    
               [day, timestamp, time_raw, time, hours_raw, hours, mass_list, intensity_list]
            2. Dictionary of timetraces: Keys: 1,2,3,4,5 (masses), time, time_raw, hours, hours_raw, timestamp
            3. Header (dictionary with keys: raw, masses)
    """
    error = []
    
    with open(file_path, 'r') as f:
        txt = f.readlines()

    head = txt[7].strip().replace(',', '.').split('\t')
    header = {}
    header['raw'] = head
    data = np.asarray([i.strip().split('\t') for i in txt[8:]])

    tra = np.transpose(data)
    '''If no data in file'''
    if len(tra) == 0:
        error.append(153)
        return [''], {}, header, error

    mass_list = []
    for i in xrange(len(head) -5):
        num = float(head[i+4].split(' ')[1])
        mass_list.append((num))
    
    data3 = []
    for i in xrange(len(data)):
        data3.append(map(float,data[i][4:-1]))

    hours = []
    time = []
    sync_time = []
    
    #Set time0 = time to first measurement point
    h0, m0, s0 = map(float,tra[1][0].split(':'))
     
    no_input_switch = True            

    j = 0
    for i in xrange(len(tra[-1])):
        j = j+1
        if tra[-1][i] == '0':
            no_input_switch = False
            break         
    h_sync, m_sync, s_sync = map(float,tra[1][j-1].split(':'))

    for i in xrange(len(tra[1])):
        h, m, s = map(float,tra[1][i].split(':'))
        #Making hours sequent
        hours.append(h + m/60. + s/3600.)

    # Sometimes data is stored with the same time: Switch second point to the mid of 2 different time steps
    for i in xrange(len(hours)- 2):
        if hours[i] >= hours[i+1]:
            hours[i+1] = (hours[i] + hours[i+2]) / 2.
    if hours[-2] >= hours[-1]:
        hours[-1] = hours[-2]+0.01/3600

    #Making time sequent: set first measurement to 0
    time = map(lambda x: x*3600., hours)
    #Making sync time sequent: set point with first value of 0 at digital input
    sync_in_sec = h_sync*3600 + m_sync*60 + s_sync
    sync_time = map(lambda x: x-sync_in_sec, time) 
    
    sync_hours = [x / 3600 for x in sync_time]
       
               
    # Creating List of Spectras    
    day = tra[0][0].split('.')[0]+'.'+tra[0][0].split('.')[1]+'.20'+tra[0][0].split('.')[2]
    list_of_spectra = []   
    for i in xrange(len(data)):
        single_spectra = [day, data[i][1],time[i], sync_time[i], hours[i], sync_hours[i], mass_list, map(float,data[i][4:-1])]
        list_of_spectra.append(single_spectra)
     
    # Creating TimeTrace       
    data3 = []
    for i in xrange(len(data)):
        data3.append(map(float,data[i][4:-1]))
    m_list_round = map(int, map(round, mass_list))
    numberofmasses = []
    j= 0
    for i in xrange(len(m_list_round)-1):
        j +=1        
        if m_list_round[i] != m_list_round[i+1]:
            #print m_list_round[i], m_list_round[i+1],j
            numberofmasses.append(j)
            j = 0
    numberofmasses.append(len(m_list_round) - sum(numberofmasses))
    #print numberofmasses, m_list_round
    it = 0
    all_mass_max = []
    for j in xrange(len(numberofmasses)):
        mass_max = []
        for i in xrange(len(data3)):
             mass_max.append(max(data3[i][it:it+numberofmasses[j]]))  
            #print min(data3[i][it:it+numberofmasses[j]])             
        it += numberofmasses[j]   
        all_mass_max.append(mass_max)
    diff_mass = f7(m_list_round)
    #print all_mass_max, diff_mass 
    header['masses'] =diff_mass
                 
    timetrace = {}
    timetrace['timestamp'] = list(tra[1])
    timetrace['time_raw'] = time
    timetrace['hours_raw'] = hours
    
    if no_input_switch:
        timetrace['time'] = timetrace['time_raw']
        timetrace['hours'] = timetrace['hours_raw']
    else:
        timetrace['time'] = map(float,sync_time)
        timetrace['hours'] = map(lambda x: x/3600., timetrace['time'])    
    
    for i in xrange(len(diff_mass)):
        timetrace[diff_mass[i]] = all_mass_max[i]


    return list_of_spectra, timetrace, header, error

#Get back a unified list, which is perserving the order
#Same as set(list), but set is not order perserving
def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

#s, t, h = read_HPQ_P('D:/RGA raw data/RGA raw data/2016/2016-04/HPQO2 Peak Jump/HPQO220160401085825.txt')
##s, t, h = read_HPQ_P('D:/RGA raw data/RGA raw data/2016/2016-04/HPQO Peak Jump/HPQO20160401081621.txt')
#
#for i in s:
#    print i
