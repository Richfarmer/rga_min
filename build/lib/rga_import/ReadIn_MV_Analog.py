import numpy as np
from datetime import datetime


def read_MV_A(file_path):

    error = []

    with open(file_path, 'r') as f:
        txt = f.readlines()

    head = txt[7].strip().replace(',', '.').split('\t')
    header = {}
    header['raw'] = head
    data = np.asarray([i.strip().split('\t') for i in txt[8:]])

    tra = np.transpose(data)
    #If no data in file
    if len(tra) == 0:
        error.append(252)
        return [''], {}, header, error

    mass_list = []
    try:
        for i in xrange(len(head) -3):
            #print head[i+3]
            num = float(head[i+2].split(' ')[1])
            mass_list.append((num))
    except:
        for i in xrange(len(head) -5):
            #print head[i+4]
            num = float(head[i+4].split(' ')[1])
            mass_list.append((num))        
    #print mass_list   
   
    hours = []
    time = []

    #Timepoint from which all timeperiods are calculated
    t_0 = datetime(2000,1,1)
    time0_sec = (datetime(int('20'+tra[0][0][6:8]),int(tra[0][0][3:5]),int(tra[0][0][0:2]),int(tra[0][0][9:11]),int(tra[0][0][12:14]),int(tra[0][0][15:17])) - t_0).total_seconds() + float('0.'+tra[0][0][18:])   

    for i in xrange(len(tra[1])):
        h, m, s = map(float,tra[0][i].split(' ')[1].split(':'))
        #Making hours sequent
        hours.append(h + m/60. + s/3600.)
        
    # If Analog data goes up to the next day add 24 hours to the hours list. 
    for i in xrange(len(hours)-1):
        if hours[i]-hours[i+1] > 23:
            hours[i+1:] = map(lambda x: x+24, hours[i+1:])

    for i in xrange(len(tra[1])):
        #Making time sequent: set first measurement to 0
        timei_sec = (datetime(int('20'+tra[0][i][6:8]),int(tra[0][i][3:5]),int(tra[0][i][0:2]),int(tra[0][i][9:11]),int(tra[0][i][12:14]),int(tra[0][i][15:17])) - t_0).total_seconds() + float('0.'+tra[0][i][18:])
        time.append(timei_sec-time0_sec)

    # Creating List of Spectras          
    list_of_spectra = []   
    for i in xrange(len(data)):
        single_spectra = [(data[i][0].split(' ')[0]).replace('/', '.'), data[i][0].split(' ')[1],time[i], time[i], hours[i], hours[i], mass_list, map(float,data[i][2:-1])]
        list_of_spectra.append(single_spectra)
   
    # Creating TimeTrace       
    data3 = []
    for i in xrange(len(data)):
        data3.append(map(float,data[i][2:-1]))
    m_list_round = map(int, map(round, mass_list))
    numberofmasses = []
    j= 0
    for i in xrange(len(m_list_round)-1):
        j +=1        
        if m_list_round[i] != m_list_round[i+1]:
            #print m_list_round[i], m_list_round[i+1],j
            numberofmasses.append(j)
            j = 0
    numberofmasses.append(len(m_list_round) - sum(numberofmasses))
    #print numberofmasses, m_list_round
    it = 0
    all_mass_max = []
    for j in xrange(len(numberofmasses)):
        mass_max = []
        for i in xrange(len(data3)):
             mass_max.append(max(data3[i][it:it+numberofmasses[j]]))  
            #print min(data3[i][it:it+numberofmasses[j]])             
        it += numberofmasses[j]   
        all_mass_max.append(mass_max)
    diff_mass = f7(m_list_round)
    #print all_mass_max, diff_mass   
    header['masses'] = diff_mass         

    timetrace = {}
    timetrace['timestamp'] = tra[0]#[x.split(' ')[1] for x in tra[0]]
    timetrace['time_raw'] = time
    timetrace['hours_raw'] = hours
    timetrace['time'] = time
    timetrace['hours'] = hours
    
    for i in xrange(len(diff_mass)):
        timetrace[diff_mass[i]] = all_mass_max[i]           

    return list_of_spectra, timetrace, header, error
    
def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]
    
#s, t, h, e= read_MV_A('/afs/ipp-garching.mpg.de/u/augd/rawfiles/MSP/2017/2017-10/HD Analog/HD20171006161201.txt')
#s, t, h, e= read_MV_A('/afs/ipp-garching.mpg.de/u/augd/rawfiles/MSP/2017/2017-10/MV Analog/MV20171006133055.txt')
#print t['time']
