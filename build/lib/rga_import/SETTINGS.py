# -*- coding: utf-8 -*-
"""
Read in the settings file of the MSPs for the data evaluation 
"""
import importlib
import datetime as dt
import os
import sys

path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(path)

settings_file = '/afs/ipp-garching.mpg.de/augd/rawfiles/MSP/device_settings/RGA_Min_settings/Settings.txt'

devices =  []
endings2 = {}
functions= {}
modules =  {}
t0 =       {}
sn_min =   {}


with open(settings_file, 'r') as f:
    txt = f.readlines()

for i in xrange(len(txt)):
    #Read out folder where raw data is saved from TXT file
    if '#MAIN_DATA_FOLDER' in txt[i]:
        MAIN_DATA_FOLDER = txt[i+1]
    #Read out endings of raw files which should be read out for each device
    #Additionally all devices are set to std. dev.
    if '#STANDARD_ENDINGS' in  txt[i]:
        for j in xrange(len(txt)-i-1):
            if txt[i+j+1][0] != '#':
                if len(txt[i+j+1].strip('\n').split('\t')) != 1:
                    info = txt[i+j+1].strip('\n').split('\t')
                    endings2[info[0]] = [info[1], info[2]]
                    devices.append(info[0])
            else:
                break
    # Read out used modules for data read in with functions
    if '#USED_MODULES' in  txt[i]:
        for j in xrange(len(txt)-i-1):
            if txt[i+j+1][0] != '#':
                if len(txt[i+j+1].strip('\n').split('\t')) != 1:
                    info = txt[i+j+1].strip('\n').split('\t')
                    modules[info[0]] = info[1]
            else:
                break
    #Sort and add used functions to funtion library
    if '#USED_FUNCTIONS' in txt[i]:
        for j in xrange(len(txt)-i-1):
            if txt[i+j+1][0] != '#':
                if len(txt[i+j+1].strip('\n').split('\t')) != 1:
                    info = txt[i+j+1].strip('\n').split('\t')
                    p = importlib.import_module(info[1])
                    method_p = getattr(p, modules[info[1]])
                    functions[info[0]] = [method_p, []]
                    if ',' in info[2]:
                        for mod in info[2].split(','):
                            a = importlib.import_module(mod)
                            functions[info[0]][1].append(getattr(a, modules[mod]))
                    else:
                        a = importlib.import_module(info[2])
                        functions[info[0]][1].append(getattr(a, modules[info[2]]))
            else:
                 break
    # Read in the min date and SN for each device 
    if '#DATA_STARTING' in txt[i]:
        for j in xrange(len(txt)-i-1):
            if txt[i+j+1][0] != '#':
                if len(txt[i+j+1].strip('\n').split('\t')) != 1:
                    info = txt[i+j+1].strip('\n').split('\t')
                    t0[info[0]] = dt.datetime(int(info[1]), int(info[2]), int(info[3]), int(info[4]), int(info[5]), 00)
                    sn_min[info[0]] = int(info[6])
            else:
                break
         
        
#print MAIN_DATA_FOLDER
#print endings2
#print modules
#print functions
#print t0
#print sn_min
