from rga_import.SETTINGS import *
import time as t

def right_input(device, date = None, time = None, shotnumber = None):
    error = []
    '''Checking if device is given as a string and if it's in the list of devices'''
    right_dev = False
    
    if type(device) == str:
        for i in devices:
            if i == device:
                right_dev = True
                
    if right_dev == False:
        error.append(350)
        return error

    '''Checking if either shotnumber or date and time is given'''
    if shotnumber in [None, '']:
        try:
            t.strptime(time, '%H:%M:%S')
            t.strptime(date, '%d.%m.%Y')
        except ValueError:
            error.append(351)
            return error
        # Is any data from the spectrometers given
        # HPQO, MV, HPQI: After 28.01.2014, 09:24:00, SN: 30151
        # HPQO2         : After 24.04.2014, 08:52:00, SN: 30627
        # HIDEN         : After 12.01.2016, 14:02:00, SN: 32819
        # HD            : After 30.01.2014, 10:50:00, SN: 30174
        # HIDEN_2       : After 05.05.2017, 09:30:00, SN: 34157
        hr, mi, se = map(int, time.split(':'))
        da, mo, yr = map(int, date.split('.'))
        
        if device in ['MV', 'HPQO', 'HPQI']:
            if dt.datetime(yr,mo,da,hr,mi,se) < t0[device]:
                error.append(352)
                return error
        if device == 'HPQO2':
            if dt.datetime(yr,mo,da,hr,mi,se) < t0[device]:
                error.append(352)
                return error
        if device == 'HIDEN':
            if dt.datetime(yr,mo,da,hr,mi,se) < t0[device]:
                error.append(352)
                return error
        if device == 'HD':
            if dt.datetime(yr,mo,da,hr,mi,se) < t0[device]:
                error.append(352)
                return error
        if device == 'HIDEN_2':
            if dt.datetime(yr,mo,da,hr,mi,se) < t0[device]:
                error.append(352)
                return error

        return error
        
            
    elif time in [None, ''] and date in [None, '']:
        if type(shotnumber) == str:
            try:
                int(shotnumber)
                if device in ['MV', 'HPQO', 'HPQI']:
                    if int(shotnumber) < sn_min[device]:
                        error.append(352)
                        return error
                if device == 'HPQO2':
                    if int(shotnumber) < sn_min[device]:
                        error.append(352)
                        return error
                if device == 'HIDEN':
                    if int(shotnumber) < sn_min[device]:
                        error.append(352)
                        return error
                if device == 'HD':
                    if int(shotnumber) < sn_min[device]:
                        error.append(352)
                        return error
                if device == 'HIDEN_2':
                    if int(shotnumber) < sn_min[device]:
                        error.append(352)
                        return error
                    
                return error
                
            except ValueError:
                try:
                    sn, steps = shotnumber.split(' ')
                    try:
                        sn = int(sn)
                        direction = steps[:1]
                        step = steps[1:]
                        if direction == '+' or direction == '-':
                            try:
                                int(step)
                                return error
                            except ValueError:
                                error.append(353)
                                return error
                        else:
                            error.append(353)
                            return error
                        
                    except ValueError:
                        error.append(353)
                        return error
                except ValueError:
                    error.append(353)
                    return error
        else:
            error.append(353)
            return error
            

    else:        
        error.extend([354])
        return error
