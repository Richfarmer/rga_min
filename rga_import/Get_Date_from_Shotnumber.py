"""
"""
import journal
import datetime

#setenv PYTHONPATH /afs/ipp/aug/ads-diags/common/python/lib
'''
INPUT: Shotnumber
OUTPUT: Date, Time of the Shot and Error message
'''

def get_date_time(shotnumber):
    error = []

    try:
        '''Is given SN too high?'''   
        lastDate = journal.getPrevSession(str(datetime.date.today()))
        entries = journal.searchSession(lastDate)
        if entries[-1]['shotno'] < shotnumber:
            date = 'N/A'
            time = 'N/A'
            error.append(151)
            return date, time, error
    except:
        error.append(251)

    '''Get date and time from shot'''
    entry = journal.getEntryForShot(shotnumber)

    time = entry['time']+':00'
    y,m,d = entry['datum'].split('-')
    date = d+'.'+m+'.'+y

    if time == ':00' or time == ' :00':
        error.append(152)

    return date, time, error
