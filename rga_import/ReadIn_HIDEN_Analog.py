# -*- coding: utf-8 -*-
import scipy as sp
import numpy as np
import os

def read_HIDEN_A(filepath):
    error = []
    
    infile = open(filepath,'r')
    rawfeed = infile.read().splitlines()
    infile.close()
    header_spot = 'N/A'
    ver = 'old'

    for line in rawfeed:
        elements = line.split(',')
        if elements[0] == '"Date"':
            date_tag = elements[1]
            time_tag = elements[3]
        if elements[0] == '"Scans"':
            num_of_scans = int(elements[1])
        if elements[0] == '"Cycle"':
            # to je že header
            header_raw = line.split(',')
            header_spot = rawfeed.index(line)
            break
    if header_spot == 'N/A':
        for line in rawfeed:
            elements = line.split(';')
            if elements[0] == '"Date"':
                date_tag = elements[1]
                time_tag = elements[3]
            if elements[0] == '"Scans"':
                num_of_scans = int(elements[1])
            if elements[0] == '"Cycle"':
                # to je že header
                header_raw = line.split(';')
                header_spot = rawfeed.index(line)
                ver = 'new'
                break

    if header_spot == 'N/A':
        #print 'WARNING: No data in HIDEN Analog File given!'
        error.append(254)
        return [], {}, {}, error
        
    data = []
    if ver == 'old':
        for line in rawfeed[header_spot + 1:]:
            data.append(line.split(','))
    if ver == 'new':
        for line in rawfeed[header_spot + 1:]:
            data.append(line.split(';'))        
    data = sp.array(data)
    cols = sp.transpose(data)
    for i in range(len(cols)):
        cols[i] = sp.array(cols[i])
    #Add additional if clause: Sometimes time_tag is not in the file
        # Use then the name of the file.
    if time_tag == '' or time_tag == ' ' or len(time_tag) != 8:
        if 'view' in os.path.split(filepath)[1]:
            time_tag = os.path.split(filepath)[1].split('.')[0][-11:-9]+':'+os.path.split(filepath)[1].split('.')[0][-9:-7]+':'+os.path.split(filepath)[1].split('.')[0][-7:-5]
        else:
            time_tag = os.path.split(filepath)[1].split('.')[0][-6:-4]+':'+os.path.split(filepath)[1].split('.')[0][-4:-2]+':'+os.path.split(filepath)[1].split('.')[0][-2:]

    if 'view' in os.path.split(filepath)[1]:
        date_tag = os.path.split(filepath)[1].split('.')[0][-13:-11]+'.'+os.path.split(filepath)[1].split('.')[0][-15:-13]+'.'+os.path.split(filepath)[1].split('.')[0][-17:-15]
    else:
        date_tag = os.path.split(filepath)[1].split('.')[0][-8:-6]+'.'+os.path.split(filepath)[1].split('.')[0][-10:-8]+'.'+os.path.split(filepath)[1].split('.')[0][-12:-10]

    hrs, mins, secs = map(int, time_tag.split(':'))
    start_secs = secs + 60 * (mins + 60 * hrs)

    cycles = sp.array(sorted(set(cols[0])))

    list_of_spectra = []
    #spectra_d = {}
    for cycle in cycles:
        area = cols[0] == cycle
        time = float(cols[2][area][0]) / 1000
        #For new data replace all ',' by '.'
        cols[3][area] = np.char.replace(cols[3][area],',','.')
        cols[4][area] = np.char.replace(cols[4][area],',','.')
        
        mass_col = sp.array(map(float,cols[3][area]))
        int_col = sp.array(map(float, cols[4][area]))
        hours = float((start_secs + time)) / 3600

        # Constructing Timestamp
        hrs = int(hours)
        minutes = 60* (hours - hrs)
        mins = int(minutes)
        secs = (60 * (minutes - mins))
        bits = str(secs).split('.')
        timestamp = "%s:%s:%s" %(str(hrs).zfill(2), str(mins).zfill(2), "%s.%s" % (bits[0].zfill(2), bits[1]))
        #spectra_d[cycle] = {'time': time, 'hours': hours, 'timestamp': [date_tag, timestamp], 'mass': mass_col, 'intensity': int_col}
        list_of_spectra.append([date_tag, timestamp, time, time, hours, hours, mass_col, int_col])

    #Constructing timetrace
    mass_col = list_of_spectra[0][-2]
    start_m = int(round(mass_col[0]))
    stop_m = int(round(mass_col[-1]))
    #print start_m, stop_m

    mass_list = []
    for mass in range(start_m, stop_m+1):
        area = (mass_col >= mass - 0.5) * (mass_col < mass + 0.5)
        sublist = mass_col[area]
        if len(sublist) > 3:                                                    #WHY BIGGER 3? RANDOM NUMBER?
            mass_list.append(mass)
            
    timetrace = {'time': [], 'hours': [], 'time_raw': [], 'hours_raw': [], 'timestamp': []}
    for mass in mass_list:
        timetrace[mass] = []
        
    for spectrum in list_of_spectra:
        date_tag2, timestamp, time_raw, time, hours_raw, hours, mass_col, int_col = spectrum
        timetrace['time'].append(time)
        timetrace['hours'].append(hours)
        timetrace['time_raw'].append(time)
        timetrace['hours_raw'].append(hours)
        timetrace['timestamp'].append(timestamp)
        for mass in mass_list:
            try:
                area = (mass_col >= mass - 0.5) * (mass_col < mass + 0.5)
                val = max(int_col[area])
            except:
                val = 0
            timetrace[mass].append(val)
    for key in timetrace.keys():
        timetrace[key] = sp.array(timetrace[key])
    
    #Wanna also have Header information in the return to have additional 
    #information about the machine
    header = {}
    
    header['masses'] = mass_list
    
    header['Date'] = date_tag
    header['Time'] = time_tag
    
    knowledge = 0.        # knowledge of how many scans were done
    
    ele = ['"F1"','"F2"','"cage"','"delta-m"','"electron-energy"','"emission"','"focus"','"mass"','"mode-change-delay"','"multiplier"','"resolution"']
    head = ['F1','F2','Cage','Delta M','Electron Energy','Emission','Focus','Mass','Mode Change Delay','Multiplier','Resolution']
    
    for line in rawfeed:
        elements = line.split(',')
        if elements[0] == '"ID"':
            header['Spectrometer ID'] = elements[1], 'Version:', elements[3]
        
        if elements[0] == '"Scans"':
            total_scans = int(elements[1])
            knowledge = 1.
            header['Total Scans'] = int(total_scans)
          
        if knowledge == 1.:
            for i in xrange(total_scans):
                if elements[0] == '"Scan {0}"'.format(i+1):
                    scan = {} 
                    scan['Mode'] = elements[1]
                    scan['Input'] = elements[2]
                    scan['Scanned'] = elements[3]
                    scan['Start'] = elements[4]
                    scan['Stop'] = elements[5]
                    scan['Increment'] = elements[6]
                    scan['Dwell'] = elements[7]
                    scan['Settle'] = elements[8]
                    scan['RS'] = elements[9]
                    scan['RelSEM'] = elements[10]
                    header['Scan {0}'.format(i+1)] = scan   
        for i in xrange(len(ele)):
            if elements[0] == ele[i]:
                header[head[i]] = elements[1], elements[2]
    
    return list_of_spectra, timetrace, header, error

#HIDEN_ANALOG = '/afs/ipp-garching.mpg.de/u/augd/rawfiles/MSP/2016/2016-02/HIDEN Analog/Raw data/HIDEN20160218082907.csv' #random csv file
#s, t, h = read_HIDEN_A(HIDEN_ANALOG)
#print s
#for i in t:
#    print i, len(t[i])
