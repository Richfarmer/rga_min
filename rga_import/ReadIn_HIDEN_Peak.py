import scipy as sp
import numpy as np
import os


def read_HIDEN_P(filepath):
    error = []
    
    infile = open(filepath,'r')
    rawfeed = infile.read().splitlines()
    infile.close()
    
    data_given = False
    ver = 'old'
    
    for line in rawfeed:
        elements = line.split(',')
        if elements[0] == '"Date"':
            date_tag = elements[1]
            time_tag = elements[3]
        if elements[0] == '"Scans"':
            num_of_scans = int(elements[1])
        if elements[0] == '"Time"':
            header_raw = line.split(',')
            header_spot = rawfeed.index(line)
            data_given = True                              
            break
    if not data_given:
        for line in rawfeed:
            elements = line.split(';')
            if elements[0] == '"Date"':
                date_tag = elements[1]
                time_tag = elements[3]
            if elements[0] == '"Scans"':
                num_of_scans = int(elements[1])
            if elements[0] == '"Time"':
                header_raw = line.split(';')
                header_spot = rawfeed.index(line)
                data_given = True
                ver = 'new'
                break
        
    #NEW: If no data given: No 'Time'                      
    if not data_given:
        #print 'WARNING: No data in HIDEN Peak File given!'
        error.append(155)
        return [], {}, {}, error
        
    data = []
    if ver == 'old':
        for line in rawfeed[header_spot + 1:]:
            data.append(line.split(','))
    if ver == 'new':
        for line in rawfeed[header_spot + 1:]:
            data.append(line.split(';')) 
    data = sp.array(data)
    cols = sp.transpose(data) # == Tra
    
    mass_list = []
    mass_index = {}
    for entry in header_raw:
        if 'mass' in entry:
            mass = float(entry[-6:-1])
            if mass.is_integer():
                mass_list.append(mass)
                mass_index[mass] = header_raw.index(entry)

    columns = {}
    columns['time_raw'] = (sp.array(map(float, cols[1])))* 1e-3
    
    for mass_i in mass_list:
        ci = mass_index[mass_i]
        mass = int(mass_i)
        cols[ci] = np.char.replace(cols[ci], ',', '.')
        columns[mass] = sp.array(map(float, cols[ci]))

    #Add additional if clause: Sometimes time_tag is not in the file
        # Use then the name of the file.
    if time_tag == '' or time_tag == ' ' or len(time_tag) != 8:
        if os.path.split(filepath)[1].split('.')[0][-5:] == 'view1':
            time_tag = os.path.split(filepath)[1].split('.')[0][-12:-10]+':'+os.path.split(filepath)[1].split('.')[0][-10:-8]+':'+os.path.split(filepath)[1].split('.')[0][-8:-6]
        else:
            time_tag = os.path.split(filepath)[1].split('.')[0][-6:-4]+':'+os.path.split(filepath)[1].split('.')[0][-4:-2]+':'+os.path.split(filepath)[1].split('.')[0][-2:]

    hr, mn, sc = time_tag.split(':')
    seconds = int(sc) + 60 *(int(mn) + 60 * int(hr))
    hour_start = float(seconds)/3600
    columns['hours_raw'] = columns['time_raw']  / 3600 + hour_start
    
    #Getting synchronisation signal: Set time 0 to first point, where TS06 jumps 
    #roughly to 5 
    no_TS06_switch = True
    TS = np.char.replace(cols[len(cols)-2],',','.')
    TS06 = sp.array(map(float, TS))
    for i in xrange(len(TS06)):
        if TS06[i] > 4 or TS06[i] < -4:   
            delta_time = float(cols[1][i])
            no_TS06_switch = False
            break
    if not no_TS06_switch:
        columns['time'] = columns['time_raw'] - delta_time*1e-3
    else:
        columns['time'] = columns['time_raw']
    #Add sync hours
    columns['hours'] = columns['time']/3600.

    # Creating List of Spectras  
    day = os.path.split(filepath)[1].split('.')[0][-13:-11]+'.'+os.path.split(filepath)[1].split('.')[0][-15:-13]+'.'+os.path.split(filepath)[1].split('.')[0][-19:-15]    
    list_of_spectra = []   
    for i in xrange(len(data)):
        single_spectra = [day, data[i][0],columns['time_raw'][i], columns['time'][i], columns['hours_raw'][i], columns['hours'][i], mass_list, map(float,data[i][2:-2])]
        list_of_spectra.append(single_spectra)
    #print list_of_spectra 
        
    hrs, mins, secs = map(int, time_tag.split(':'))
    timestamp = []
    # Constructing Timestamp
    for se in cols[1]:
        tot = hrs*3600. + mins*60 + secs + float(se)/1000.
        h = int(tot/3600)
        m = int((tot-h*3600)/60)
        s = (tot - h*3600 - m*60)
        bits = str(s).split('.')
        timestamp.append("%s:%s:%s" %(str(h).zfill(2), str(m).zfill(2), "%s.%s" % (bits[0].zfill(2), bits[1])))
       
    # Creating TimeTrace                   
    timetrace = {}
    timetrace['timestamp'] = timestamp
    timetrace['time_raw'] = (columns['time_raw'])
    timetrace['hours_raw'] = columns['hours_raw']
    timetrace['time'] = columns['time']
    timetrace['hours'] = columns['hours']
    for i in xrange(len(mass_list)):
        timetrace[int(mass_list[i])] = columns[mass_list[i]]

    #Wanna also have Header information in the return to have additional 
    #information about the machine
    header = {}
    
    header['masses'] = mass_list
    
    header['Date'] = date_tag
    header['Time'] = time_tag
    
    knowledge = 0.        # knowledge of how many scans were done
    
    ele = ['"F1"','"F2"','"cage"','"delta-m"','"electron-energy"','"emission"','"focus"','"mass"','"mode-change-delay"','"multiplier"','"resolution"']
    head = ['F1','F2','Cage','Delta M','Electron Energy','Emission','Focus','Mass','Mode Change Delay','Multiplier','Resolution']
    
    for line in rawfeed:
        elements = line.split(',')
        if elements[0] == '"ID"':
            header['Spectrometer ID'] = elements[1], 'Version:', elements[3]
        
        if elements[0] == '"Scans"':
            total_scans = int(elements[1])
            knowledge = 1.
            header['Total Scans'] = int(total_scans)

            
        if knowledge == 1.:
            for i in xrange(total_scans):
                if elements[0] == '"Scan {0}"'.format(i+1):
                    scan = {} 
                    scan['Mode'] = elements[1]
                    scan['Input'] = elements[2]
                    scan['Scanned'] = elements[3]
                    scan['Start'] = elements[4]
                    scan['Stop'] = elements[5]
                    scan['Increment'] = elements[6]
                    scan['Dwell'] = elements[7]
                    scan['Settle'] = elements[8]
                    scan['RS'] = elements[9]
                    scan['RelSEM'] = elements[10]
                    header['Scan {0}'.format(i+1)] = scan   
        for i in xrange(len(ele)):
            if elements[0] == ele[i]:
                header[head[i]] = elements[1], elements[2]

 
    return list_of_spectra, timetrace, header, error

#HIDEN_PEAK = 'D:/RGA raw data/RGA raw data/2016/2016-04/HIDEN Peak Jump/Raw data/HIDEN20160401115753view1.csv' #random csv file
##HIDEN_PEAK = 'D:/RGA raw data/RGA raw data/2016/2016-05/HIDEN Peak Jump/Raw data/HIDEN20160512103949view1.csv' #random csv file
#s, t, h = read_HIDEN_P(HIDEN_PEAK)
#
#for i in s:
#    print i
