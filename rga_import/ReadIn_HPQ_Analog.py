import numpy as np
from datetime import datetime

def read_HPQ_A(file_path):
    error = []
  
    with open(file_path, 'r') as f:
        txt = f.readlines()

    head = txt[7].strip().replace(',', '.').split('\t')
    header = {}
    header['raw'] = head
    data = np.asarray([i.strip().split('\t') for i in txt[8:]])

    tra = np.transpose(data)
    if len(tra) == 0:
        error.append(253)
        return [''], {}, header, error
        

    mass_list = []
    for i in xrange(len(head) -5):
        num = float(head[i+4].split(' ')[1])
        mass_list.append((num))
    #print mass_list
        
    hours = []
    time = []

    #Timepoint from which all timeperiods are calculated
    t_0 = datetime(2000,1,1)

    time0_sec = (datetime(int('20'+tra[0][0][6:]),int(tra[0][0][3:5]),int(tra[0][0][0:2]),int(tra[1][0][0:2]),int(tra[1][0][3:5]),int(tra[1][0][6:8])) - t_0).total_seconds() + float('0.'+tra[1][0][9:])   

    for i in xrange(len(tra[1])):
        h, m, s = map(float,tra[1][i].split(':'))
        #Making hours sequent
        hours.append(h + m/60. + s/3600.)
        
    # If Analog data goes up to the next day add 24 hours to the hours list. 
    for i in xrange(len(hours)-1):
        if hours[i]-hours[i+1] > 23:
            hours[i+1:] = map(lambda x: x+24, hours[i+1:])

    for i in xrange(len(tra[1])):
        #Making time sequent: set first measurement to 0
        timei_sec = (datetime(int('20'+tra[0][i][6:]),int(tra[0][i][3:5]),int(tra[0][i][0:2]),int(tra[1][i][0:2]),int(tra[1][i][3:5]),int(tra[1][i][6:8])) - t_0).total_seconds() + float('0.'+tra[1][i][9:])
        time.append(timei_sec-time0_sec)

    # Creating List of Spectras          
    list_of_spectra = []   
    for i in xrange(len(data)):
        single_spectra = [data[i][0],data[i][1],time[i], time[i], hours[i], hours[i], mass_list, map(float,data[i][4:-1])]
        list_of_spectra.append(single_spectra)
     
    # Creating TimeTrace       
    data3 = []
    for i in xrange(len(data)):
        data3.append(map(float,data[i][4:-1]))
    m_list_round = map(int, map(round, mass_list))
    numberofmasses = []
    j= 0
    for i in xrange(len(m_list_round)-1):
        j +=1        
        if m_list_round[i] != m_list_round[i+1]:
            #print m_list_round[i], m_list_round[i+1],j
            numberofmasses.append(j)
            j = 0
    numberofmasses.append(len(m_list_round) - sum(numberofmasses))
    #print numberofmasses, m_list_round
    it = 0
    all_mass_max = []
    for j in xrange(len(numberofmasses)):
        mass_max = []
        for i in xrange(len(data3)):
             mass_max.append(max(data3[i][it:it+numberofmasses[j]]))  
            #print min(data3[i][it:it+numberofmasses[j]])             
        it += numberofmasses[j]   
        all_mass_max.append(mass_max)
    diff_mass = f7(m_list_round)
    #print all_mass_max, diff_mass  
    header['masses'] = diff_mass          
        
    timetrace = {}
    timetrace['timestamp'] = list(tra[1])
    timetrace['time_raw'] = time
    timetrace['hours_raw'] = hours
    timetrace['time'] = time
    timetrace['hours'] = hours

    for i in xrange(len(diff_mass)):
        timetrace[diff_mass[i]] = all_mass_max[i]
        
          
    return list_of_spectra, timetrace, header, error

#Get back a unified list, which is perserving the order
#Same as set(list), but set is not order perserving
def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

##s, t, h = read_HPQ_A('D:/RGA raw data/RGA raw data/2016/2016-04/HPQO Analog/HPQO20160401081655.txt')
#s, t, h = read_HPQ_A('/afs/ipp-garching.mpg.de/u/augd/rawfiles/MSP/2016/2016-04/HPQO2 Analog/HPQO220160401131515.txt')
#
#for i in s:
#    print i
