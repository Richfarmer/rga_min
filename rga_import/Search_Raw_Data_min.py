import fnmatch
import os
import datetime
from SETTINGS import *

'''
INPUT:
DATE1 and TIME1:    NEEDED VALUES
DEVICES:            'HPQO', 'HPQO2', 'HPQI', 'MV', 'HIDEN', STANDARD: Data of all devices are searched
                    Input always as a String: ['HPQO'], or also 2 or 3 devices possible: ['HPQO', 'MV']
ENDINGS:            Always standard import. Don't change here.
TIME-SETTINGS:      Can change timesettings which are usually in a standard mode.

OUTPUT:
2 Dictionaries called peak_files and analog_files with:
    Keys:   Device names
    Values: List of storage locations for all appropiate files

Time sequence has to be in one month!
'''

def search_files(date1 = None, time1 = None, devices = devices, max_timeoffset_shottopeak = 480, max_timeoffset_peaktoanalog = 200):

    '''Settings Lists of File Directories for all files which are given back'''
    filedirectory_peak = {}
    filedirectory_analog = {}
    err = {}
    
    for device in devices:
        filedirectory_peak[device], filedirectory_analog[device], err[device] =  search_files_for_one_device(device, date1, time1, max_timeoffset_shottopeak, max_timeoffset_peaktoanalog)          
                    
    return filedirectory_peak, filedirectory_analog, err
   

'''
Searches for Peak files in given timewindow: date,time - date,time+timeoffset
'''
def search_peak(device, date1, time1, max_timeoffset_shottopeak = 480):   
    error = []
    day1 = date1.split('.') 
    clock1 = time1.split(':')
    date_time1 =day1 + clock1
    
    t_0 = datetime.datetime(2000,1,1)

    subfolder = []
    
    '''Create folder structure'''
    folder_peak = os.path.join(MAIN_DATA_FOLDER,'{0}'.format(date_time1[2]), '{1}-{0}'.format(date_time1[1],date_time1[2]), device +' Peak Jump')
    
    ''' For Peak and Analog seperatly: Filter all matching data'''
    all_data = []
    for dirpath, dirnames, files in os.walk(folder_peak):
        all_data = fnmatch.filter(files, '*'+endings2[device][0])
        if all_data != []:
            subfolder.append(dirpath)

    '''Calculate time period from 01.01.2000 to measurement time in seconds'''
    seconds_date_of_data = {}
    x = len(device)
    for i in all_data:
        seconds_date_of_data[i] = (datetime.datetime(int(i[x:x+4]),int(i[x+4:x+6]),int(i[x+6:x+8]),int(i[x+8:x+10]),int(i[x+10:x+12]),int(i[x+12:x+14])) - t_0).total_seconds()
        
    '''Calculate time difference between wanted time and measurement time'''
    diff_time_data = {}
    for d in seconds_date_of_data:
        diff_time_data[d] = seconds_date_of_data[d] - (datetime.datetime(int(date_time1[2]),int(date_time1[1]),int(date_time1[0]),int(date_time1[3]),int(date_time1[4]),int(date_time1[5])) - t_0).total_seconds()

    ''' Having a look for the closest data to given timepoint'''
    time_diff = 1e10
  
    for d in seconds_date_of_data:
        if diff_time_data[d] < time_diff and diff_time_data[d] >= -10:  #Sometimes data is saved a few seconds earlier than the time which is given in the journal
            time_diff = diff_time_data[d]
            searched_data = d
            searched_time = seconds_date_of_data[d]
            
    '''If no data found at all or data not in time window: ERROR'''
    if time_diff > max_timeoffset_shottopeak:
        error.append(150)
        searched_data = ['N/A']
        searched_time = [1e10]

                
    return searched_data, subfolder, searched_time, error


'''
Searches for Analog files if the search for a Peak file was successful
'''
def search_analog(device, searched_time, max_timeoffset_peaktoanalog = 200):
    error = []
    t_0 = datetime.datetime(2000,1,1)
    t = t_0 + datetime.timedelta(seconds = searched_time)

    date_time1 = [str(t.day), str(t.month), str(t.year), str(t.hour), str(t.minute), str(t.second)]
    
    '''Create folder structure'''
    folder_analog = os.path.join(MAIN_DATA_FOLDER,'{0}'.format(date_time1[2]), '{1}-{0}'.format(date_time1[1].zfill(2),date_time1[2]), device +' Analog')

    subfolder = []
 
    '''Filter all matching data'''
    all_data = []
    for dirpath, dirnames, files in os.walk(folder_analog):
        all_data = fnmatch.filter(files, '*'+endings2[device][1])
        if all_data != []:
            subfolder.append(dirpath)
            
    '''Calculate time period from 01.01.2000 to measurement time in seconds'''
    seconds_date_of_data = {}
    x = len(device)
    for i in all_data:
        seconds_date_of_data[i] = (datetime.datetime(int(i[x:x+4]),int(i[x+4:x+6]),int(i[x+6:x+8]),int(i[x+8:x+10]),int(i[x+10:x+12]),int(i[x+12:x+14])) - t_0).total_seconds()
    
    '''Calculate time difference between wanted time and measurement time'''
    diff_time_data = {}
    for d in seconds_date_of_data:
        diff_time_data[d] = seconds_date_of_data[d] - (datetime.datetime(int(date_time1[2]),int(date_time1[1]),int(date_time1[0]),int(date_time1[3]),int(date_time1[4]),int(date_time1[5])) - t_0).total_seconds()

    ''' Having a look for the closest data to given timepoint'''
    time_diff = 1e10
    searched_data = []
    searched_time = []
    for d in seconds_date_of_data:
        if diff_time_data[d] < time_diff and diff_time_data[d] >= 0:
            time_diff = diff_time_data[d]
            
    '''If no data found at all or data not in time window: ERROR'''               
    if time_diff > max_timeoffset_peaktoanalog:
        error.append(250)
        searched_data = ['N/A']
        searched_time = [1e10]
    else:
        '''If data found: having a look for additional views (HIDEN: usually same time point)'''
        for d in seconds_date_of_data:
            if abs(diff_time_data[d] - time_diff) <= 3: # Max. time difference 3 seconds.
                searched_data.append(d)
                searched_time.append(diff_time_data[d])
                
                
    return searched_data, subfolder, searched_time, error


def search_files_for_one_device(device, date1, time1, max_timeoffset_shottopeak = 480, max_timeoffset_peaktoanalog = 200):
    data_p, fol_p, time_p, error_p = search_peak(device, date1, time1, max_timeoffset_shottopeak)
    if error_p == []:
        data_a, fol_a, time_a, error_a = search_analog(device, time_p, max_timeoffset_peaktoanalog)
    else:
        return ['N/A'], ['N/A'], error_p

    '''Setting up data with exact location for return''' 
    filedirectory_peak = []
    filedirectory_analog = []
  
    filedirectory_peak.append(os.path.join(fol_p[0], data_p))
    for i in data_a:
        if i == 'N/A':
            filedirectory_analog.append('N/A')
        else:
            filedirectory_analog.append(os.path.join(fol_a[0], i))
                
    return filedirectory_peak, filedirectory_analog, error_p + error_a



#print search_files_for_one_device('HIDEN_2', '12.10.2017', '13:40:00')    
#print search_files('12.10.2017', '13:40:00')
