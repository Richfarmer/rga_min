from distutils.core import setup

setup(
    name='RGA Minimal',
    version='final_1.0.1',
    packages=['rga_import',],
    py_modules=['RGA_Min'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
) 
